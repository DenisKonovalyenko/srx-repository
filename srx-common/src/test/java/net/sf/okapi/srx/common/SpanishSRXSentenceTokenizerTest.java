/* LanguageTool, a natural language style checker 
 * Copyright (C) 2005 Daniel Naber (http://www.danielnaber.de)
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 */
package net.sf.okapi.srx.common;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.sf.okapi.srx.languagetool.SRXSentenceTokenizer;
import net.sf.okapi.srx.okapi.OkapiSentenceTokenizer;

public class SpanishSRXSentenceTokenizerTest {

  private final SentenceTokenizer okapiTokenizer = new OkapiSentenceTokenizer("es");  
  private final SentenceTokenizer segmentTokenizer = new SRXSentenceTokenizer("es");
    
  private SentenceTokenizer tokenizer;

  @Before
  public void setUp() {
  }
  
  @Test
  public void okapiSegmentTest() {
	  tokenizer = okapiTokenizer;
	  segment();
  }

  @Ignore("Subtle differences with Segmenter SRX engine mean these will fail. Only Test Okapi SRX engine now")
  public void segmentTest() {
	  tokenizer = segmentTokenizer;
	  segment();
  }
  
  // NOTE: sentences here need to end with a space character so they
  // have correct whitespace when appended:
  private void segment() {
	    split("¡Buenas Tardes!");
	    split("¿Y Tú?");
	    split("¡Vaya Ud Derecho!", " !Pues Tuerza Ud por la Izquierda/ Derecha!");
	    split("Son las diez en punto.", " Las siete y media.");
	    split("Un área importante de los aeropuertos es el \"centro de control de área\", en el cual se desempeñan los controladores del tráfico aéreo; personas encargadas de dirigir y controlar el movimiento de aeronaves en el aeropuerto y en la zona bajo su jurisdicción.");
	    split("El mayor aeropuerto del mundo es el Aeropuerto Rey Khalid, en Arabia Saudita con un área total de 225 kilómetros cuadrados.");
	    split("He aquí cómo ocurrió:", " En esa época vivía en el Perú un joven príncipe, hermoso y gallardo.");
	    split("La presentación estuvo a cargo del abogado Gabriel Claudio Chamarro y denunció los delitos de \"abuso de autoridad\", \"violación de los deberes de funcionario público\" y \"malversación de caudales públicos\".");
	    split("Pues tuerza Ud. por la S.A. izquierda derecha!");	  
  }
  
  private void split(String... sentences) {
	  SrxTestUtils.testSplit(sentences, tokenizer);
  }
  
}
