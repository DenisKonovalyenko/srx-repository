<languagerule languagerulename="Shona">
<!--titles - SHONA-->
<rule break="no">
<beforebreak>\b([Dd]r|[Vv]a|[Mm]ut|[Pp]rof|[Bb]hur|[Mm]abhur|[Ss]is)\.</beforebreak>
<afterbreak>\s\P{Ll}</afterbreak>
</rule>
<!--titles-->
<rule break="no">
<beforebreak>\b([Hh]on|[Dd]r|[Mm]r|[Mm]s|[Mm]rs|[Ss]t|[Gg]en|[Cc]ol|[Mm]aj|[Bb]rig|[Ss]gt|[Cc]apt|[Cc]mnd|[Ll]t|[Ss]en|[Rr]ev|[Rr]ep|[Rr]evd|[Pp]r|[Bb]r|[Pp]res|[Pp]rof|JS|[Jj]r|[Ss]r)\.[,|:]?</beforebreak>
<afterbreak>\s\P{Ll}</afterbreak>
</rule>
<!--days of the week - SHONA-->
<rule break="no">
<beforebreak>\b([Ss]vo|[Mm]uvh|[Cc]hip|[Cc]hit|[Cc]hin|[Cc]hish|Mm]ug)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--days of the week-->
<rule break="no">
<beforebreak>\b([Ss]un|[Mm]on|[Tt]ue|[Tt]ues|[Ww]ed|[Ww]eds|[Tt]hu|[Tt]hur|[Tt]hurs|[Ff]ri|[Ss]at)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--months - SHONA-->
<rule break="no">
<beforebreak>\b([Nn]di|[Kk]uk|[Kk]ur|[Kk]ubv|[Cc]hiv|[Cc]hik|[Cc]hikung|[Nn]yam|[Gg]uny|[Gg]um|[Mm]bu|[Zz]vi)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--months-->
<rule break="no">
<beforebreak>\b([Jj]an|[Ff]eb|[Mm]ar|[Aa]pr|[Jj]un|[Jj]ul|[Aa]ug|[Ss]ep|[Ss]ept|[Oo]ct|[Nn]ov|[Dd]ec)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--scriptures - SHONA-->
<rule break="no">
<beforebreak>\b([Gg]en|[Ee]ks|[Rr]ev|[Nn]um|[Dd]eut|[Jj]osh|[Vv]at|[Ss]am|[Mm]adz|[Mm]ak|[Nn]eh|[Ee]st|[Mm]pi|[Zz]vir|[Mm]upa|[Rr]wiyo|[Ii]sa|[Jj]er|[Mm][Jj]er|[Ee]zk|[Dd]an|[Hh]os|[Jj]oere|[Aa]mosi|[Oo]bad|[Jj]ona|[Nn]ah|[Hh]ab|[Zz]ef|[Hh]ag|[Zz]ek|[Mm]ara|[Mm]at|[Jj]oh|[Mm]abasa|[Vv]a[Rr]|[Vv]a[Kk]|[Vv]a[Gg]|[Vv]a[Ee]|[Vv]a[Ff]|[Vv]a[Kk]oro|[Vv]a[Tt]|[Tt]im|[Ff]ir|[Vv]ah|[Jj]kb|[Pp]et|[Jj]ud|[Zz]vaka|[Bb]h|ra[Mm]o|[Nn]i|[Jj]ak|[Ee]no|[Jj]ar|[Oo]mu|[Aa]ru|[Hh]ir|[Mm]orm|[Ee]ta|[Mm]oro|re[Mm]u|[Mm]os|[Aa]br|[Mm]i|ye[Cc]hi)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--scriptures-->
<rule break="no">
<beforebreak>\b([Gg]en|[Ee]x|[Ll]ev|[Nn]um|[Dd]eut|[Jj]osh|[Jj]udg|[Nn]eh|[Ee]sth|[Pp]s|[Pp]rov|[Ee]ccl|[Ii]sa|[Jj]er|[Ll]am|[Ee]zek|[Dd]an|[Oo]bad|[Hh]ab|[Zz]eph|[Hh]ag|[Zz]ech|[Mm]al|[Mm]att|[Rr]om|[Cc]or|[Gg]al|[Ee]ph|[Pp]hilip|[Cc]ol|[Tt]hes|[Tt]im|[Pp]hilem|[Hh]eb|[Pp]et|[Jj]n|[Rr]ev|[Nn]e|[Hh]el|[Mm]orm|[Mm]oro|[Aa]br|[Ss]am|[Kk]gs|[Cc]hr)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--Two initialized names, e.g. "B. H. Roberts", 1-->
<rule break="no">
<beforebreak>\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\.\s[\p{Lu}$]</afterbreak>
</rule>
<!--Two initialized names, e.g. "B. H. Roberts", 2-->
<rule break="no">
<beforebreak>\p{Lu}\.\s\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\p{L}+</afterbreak>
</rule>
<!--initialized middle name - e.g. "David O. McKay";also place-names - e.g. "Washington, D. C."-->
<rule break="no">
<beforebreak>\b\p{Lu}\p{L}+,?\s\p{Lu}\.</beforebreak>
<afterbreak>\s</afterbreak>
</rule>
<!--initialized first name - e.g. "L. Tom Perry"-->
<rule break="no">
<beforebreak>\b\p{Lu}\.</beforebreak>
<afterbreak>\s\p{Lu}\p{L}+\s\p{Lu}\p{L}+</afterbreak>
</rule>
<!--common abbreviations - SHONA-->
<rule break="no">
<beforebreak>\b([Yy]unihv|[Nn]ezvi)\,</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--common abbreviations-->
<rule break="no">
<beforebreak>\b(Inc|Ltd|Corp|DC|US|lds|pm|am|[Nn]o|pp|p|[Ff]igs?|[Dd]ept|[Gg]ovt|[Ii]bid|comp|[Bb]ros|Dist|Co|Ph\.?D|et\b\s\bal|etc)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--common abbreviations - never sentence-final - SHONA-->
<rule break="no">
<beforebreak>\b([Mm]ae|[Ss]emue|[Pp]am|[Pp]as|[Mm]uny|[Nn]ha|[Mm]anha|[Pp]|[Mm]ap|[Vv]hor|[Mm]avhor|[Nn]di|[Mm]ufa|[Mm]ifa|[Zz]ving|[Kk]usang|[Kk]azh|[Dd]hipa|[Hh]urum)\.</beforebreak>
<afterbreak>\s\P{Lu}</afterbreak>
</rule>
<!--common abbreviations - never sentence-final-->
<rule break="no">
<beforebreak>\b(i\.?e|e\.?g|c\.?f|max|min|sel|ed|www|comp|[Vv]ols?|[Vv]s|[Vv]iz|[Aa]pprox|[Ii]ncl|est|B\.[SA])\.[,;:]?</beforebreak>
<afterbreak>\s</afterbreak>
</rule>
<!--country names 1-->
<rule break="no">
<beforebreak>\b([UuSs]\.)</beforebreak>
<afterbreak>\s[KkSsAa]\.</afterbreak>
</rule>
<!--country names 2-->
<rule break="no">
<beforebreak>\b([Uu]\.?\s*[Kk]|[Uu]\.?\s*[Ss]\.?\s*[Aa]?)\.</beforebreak>
<afterbreak>\s[^\p{Lu}|\p{Pi}|\p{Ps}|"]</afterbreak>
</rule>
<rule break="no">
<beforebreak>([0-9]+\.[0-9]+|[0-9]+\.[0-9]*|[0-9]*\.[0-9]+)</beforebreak>
<afterbreak>\s[^\p{Ps}\p{Pi}"'\(\[\{\p{Lu}]</afterbreak>
</rule>
<!--numbered lists (e.g. footnotes)-->
<rule break="no">
<beforebreak>^\s*[0-9]+\.</beforebreak>
<afterbreak>\s\p{Lu}</afterbreak>
</rule>
<!--manual section numbers (e.g. 17.2.3 Administering)-->
<rule break="no">
<beforebreak>[0-9]+\.[0-9]+\.</beforebreak>
<afterbreak>[0-9]+\s+\P{Ll}</afterbreak>
</rule>
<!--manual section headings 2-->
<rule break="no">
<beforebreak>[0-9]+\.[0-9]+\.[0-9]+</beforebreak>
<afterbreak>\s+\P{Ll}</afterbreak>
</rule>
<!--Roman numerals-->
<rule break="no">
<beforebreak>^[ivxIVX]+\s*\.</beforebreak>
<afterbreak>\s*[\p{Pi}\p{Ps}"'\(\[\{]*\p{Lu}</afterbreak>
</rule>
<!--A. D. / B. C.-->
<rule break="no">
<beforebreak>[AaBb]\.</beforebreak>
<afterbreak>\s[CcDd]\.</afterbreak>
</rule>
<!--general sentence final punctuation (incl. quotation marks)-->
<rule break="yes">
<beforebreak>[\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}]+\s*[\p{Pe}\p{Pf}\p{Po}"'"'‘’“”]*\s*[\.?!]+\s*[\p{Pe}\p{Pf}\p{Po}"'"'‘’“”]*</beforebreak>
<afterbreak>\s+['"\p{Pi}\(]*\p{Lu}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[.?!]+("|'|‘|’|“|”)[.?!]+</beforebreak>
<afterbreak>\s+[\p{Pi}"'\(\[\{&lt;]*\p{Lu}</afterbreak>
</rule>
<!--ellipsis character w/ punctuation before or after-->
<rule break="yes">
<beforebreak>[.!?]?['"\p{Pf}\p{Pe}]?\s*(\u2026|\.\s*\.\s*\.\s*)|(\u2026|\.\s*\.\s*\.)\s*[;.!?]?['"\p{Pf}\p{Pe}]?</beforebreak>
<afterbreak>\s*[\p{Pi}\p{Ps}\(\[\{&lt;]*\p{Lu}</afterbreak>
</rule>
<!--footnote references 1-->
<rule break="yes">
<beforebreak>\p{Ll}{2,}[.?!][\p{Pe}\p{Pf}"']{0,3}[0-9]{1,2}</beforebreak>
<afterbreak>\s\s?\p{Lu}</afterbreak>
</rule>
<!--footnote references 2-->
<rule break="yes">
<beforebreak>\p{Ll}{2,}[\p{Pe}\p{Pf}"']{0,3}[.!?][0-9]{1,2}</beforebreak>
<afterbreak>\s\s?\p{Lu}</afterbreak>
</rule>
<!--ellipsis following terminal punctuation-->
<rule break="yes">
<beforebreak>[.?!][\s\p{Pe}\p{Pf}\]\}\)]*</beforebreak>
<afterbreak>\u2026</afterbreak>
</rule>
<!--ellipsis followed by opening bracket-->
<rule break="yes">
<beforebreak>\u2026</beforebreak>
<afterbreak>\s*[\[\{\(]+\s*</afterbreak>
</rule>
<!--ellipsis followed by closing bracket-->
<rule break="yes">
<beforebreak>\u2026\s*[\]\}\)]+</beforebreak>
<afterbreak>\s*</afterbreak>
</rule>
<!--inline scripture references-->
<rule break="yes">
<beforebreak>[.?!]['"\p{Pe}\p{Pf}]*</beforebreak>
<afterbreak>\s*[\p{Pi}\p{Ps}\[\{\(]([1-5] )?\p{Lu}</afterbreak>
</rule>
<!--copyright statement-->
<rule break="yes">
<beforebreak>[.?!][\p{Pe}\p{Pf}"']*\s*</beforebreak>
<afterbreak>©\s*[0-9]{4}</afterbreak>
</rule>
<rule break="yes">
<beforebreak>[?!]</beforebreak>
<afterbreak>\p{Lu}</afterbreak>
</rule>
<!--newline-->
<rule break="yes">
<beforebreak>\n</beforebreak>
<afterbreak></afterbreak>
</rule>
</languagerule>
